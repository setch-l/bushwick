Events = new Mongo.Collection("concerts");
meteor.startup(function () {
  Events.insert({
    "name": "Pine Box"
    "date": "10/12",
    "artist": "Mr. Gnome"
  });
});
if (Meteor.isClient) {
  // counter starts at 0
  Session.setDefault("counter", 0);

  Template.bw_venue_view.helpers({
    name: "paperbox"
  });

  Template.bwmain.events({
    'click button': function () {
      // increment the counter when button is clicked
      Session.set("counter", Session.get("counter") + 1);
    }
  });
}


if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
